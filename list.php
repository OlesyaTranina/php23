<?php
$testArr = glob(__dir__ . "/files/*.json");
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задание php 22</title>
</head>
<body>
	<h2>Список загруженных тестов</h2>
	<ul>
		<?php
		foreach ($testArr as &$test){
			$info = pathinfo($test);

			?>
			<li><a href=<?= "test.php?test=" . $info["filename"] ?>>тест № <?=$info["filename"] ?></a></li>
			<?php  }
			?>
		</ul>
		<?php
		if (isset($_GET["loadnumber"])) {
			?>
			<p>Ваш тест загружен  <strong><?= "№" . $_GET["loadnumber"]  ?> </strong>
			</p>
			<?php
		}
		?>
		<a href="admin.php">Вернуться к форме выбора файла</a>
	</body>
	</html>