<?php
//проверяем это проверка выполненного теста или загрузка нового и определяем какой тест используется
if (!isset($_POST["submit"])) {
	if (isset($_GET["test"])) {
		$test = $_GET["test"]; 
	}
} else {
	$test = $_POST["test"]; 
};	
//загружаем информацию о тесте
$json_str = @file_get_contents("files/" . $test . ".json");

if (!$json_str) {
	header("HTTP/1.0 404 Not Found");
	echo "<h1>Файл теста № ". $test. " не найден.</h1>";
	echo "<a href=\"admin.php\">Вернуться к форме выбора файла</a>";
	die();
};
$json_obj = json_decode($json_str,true);
//проверка ответов если это проверка теста
if (isset($_POST["submit"])) {
//	$result = true;
	$result = 0;
	foreach ($json_obj as &$question) {
		if ($_POST["id".$question["Id"]] == $question["answer"]) {
		 ++$result;
	  }
	};
  
	$result = $result / count($json_obj) * 100;
	if ($result > 25) {
		$result_header = "Тест пройден. Вы ответили правильно на  " . $result . "% вопросов " ;
	} else {
		$result_header = "Тест не пройден. Вы ответили правильно на  " . $result . "% вопросов " ;
	};
};
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title> ДЗ 2.2 Тест № <?= $test?> </title>
</head>
<body>
	<form action="test.php" method="post">
		<lable for="userName">Ваше имя:</lable>
		<input class="userName" type="text" name="userName" value =<?php if (isset($_POST["submit"])) { echo $_POST["userName"];}; ?>>	
		<hr>			
		<h2>Tест <?= $test ?></h2>
		<?php
		foreach ($json_obj as &$question) {
			?>	
			<p><?= htmlspecialchars($question["question"]) ?>
				<input type="text" name=<?="id" . $question["Id"] ?> value=<?php if (isset($_POST["submit"])) { echo $_POST["id" . $question["Id"]];}; ?>>
				<?php if (isset($_POST["submit"])) { echo " правильный ответ: ". $question["answer"];}; ?>
			</p>		
				<?php	
			}	
			?>		
			<input type="hidden" name="test" value=<?= $test ?>>
			<?php if (isset($_POST["submit"])) {
				$sertifImgstring = "certificate.php?name=" . $_POST["userName"] . "&" . "ball=" . $result;
				?>
			<h3><?= $result_header ?></h3>
			<?php
			  if ($result > 25) {
			  ?>
			  <img src=<?=$sertifImgstring?>>
			  <?php	
			  }
			?>	
			 <?php	
			} else {
				?>
				<input type="submit" name="submit" value="Отправить на проверку">
				<?php	
			}
			?>
	</form>
	<a href="admin.php">Вернуться к форме выбора файла</a>
</body>
</html>
